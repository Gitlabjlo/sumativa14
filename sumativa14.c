#include <stdio.h>



int hermite(int n, double x);


int main()  
{  
      int a;
      int b;
      int r;
      printf ("\tSumativa14: Esta aplicacion resuelve un polinomio de Hermite.\n\n ");
      printf ("\tEscriba el grado del polinomio: ");
	  scanf ("%d",&a);
	  printf ("\tEscriba el numero: ");
	  scanf("%d",&b);
    
      r=hermite(a,b);
      printf("\tEl resultado es: %d \n", r);
      printf("Esta aplicacion fue el aborada por JOSE LORENZO 5-705-319");
    return 0;  
}


int hermite(int n, double x)  
{  
    if(n<=0)  
	{return 1;}
    
    else if(n==1)  
	{return 2*x;}
     
    else {
	
    return     2*x*hermite(n-1,x)-2*(n-1)*hermite(n-2,x);}
}  
